import { Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private nativeStorage: NativeStorage,
    private router:Router

  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.setRoot();

    });
  }

  // async onStart(){
  //   const state =  await this.nativeStorage.getItem("state");
  //   console.log(state);
  //   if(state==true){
  //     this.router.navigate(['tabs']);
  //   }else{
  //     this.router.navigate(['signup']);
  //   }

  // }

  setRoot() {
   
console.log( this.nativeStorage.getItem('state'));
    this.nativeStorage.getItem('state')
      .then(res => {
        console.log(JSON.stringify(res));
        if (res) {
          this.router.navigateByUrl('/tabs');
        }
      })
      .catch(error => {
        this.router.navigateByUrl('/signup');
      });
  }
}
