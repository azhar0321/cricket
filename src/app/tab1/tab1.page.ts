import { Component } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  getValue: any;
  value

 
  constructor(private router:Router,public route: ActivatedRoute) {
    
  }

  ngOnInit() {


    
    let id = this.route.snapshot.paramMap.get('id');
    console.log("id=",id);
    this.value=id
    
    
    
 
  }
  // signup(){
  //   this.router.navigate(['signup']);
  // }

}
