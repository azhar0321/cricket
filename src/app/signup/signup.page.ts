import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  email:string
  password:string

  constructor(private router:Router,public nativeStorage:NativeStorage) { }

  ngOnInit() {
  }
  signup(){

    if(this.email==null){
      alert("enter email");
    } else if(this.password==null){
      alert("enter password")
    }  else{

    let myArray=[this.email,this.password];
    this.nativeStorage.setItem('data_key', JSON.stringify(myArray));


      // this.nativeStorage.setItem("email",this.email);
      // this.nativeStorage.setItem("password",this.password);
     
    this.router.navigate(['login']);
    alert("Signup Sucessully");
    console.log("values",myArray);
    }
  }
  login(){
    this.router.navigate(['login']);
  }

}
